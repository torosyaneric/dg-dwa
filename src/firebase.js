import Rebase from "re-base";
import * as firebase from "firebase";
import "firebase/firestore";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyCWY0ho9jwwZlnEO9OrIsrHfcB-r2JOkFw",
  authDomain: "dg-vis.firebaseapp.com",
  projectId: "dg-vis",
  databaseURL: "https://dg-vis.firebaseio.com",
  storageBucket: "gs://dg-vis.appspot.com/"
});

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp };

export default base;
