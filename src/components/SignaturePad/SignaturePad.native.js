import React from "react";
import { Text, View } from "react-native";
import SignaturePadEl from "react-native-signature-pad";
import StyledButton from "../StyledButton";

/**
 * @return {null}
 */
function SignaturePadNative({ error, change, toggleShow, open }) {
  return open ? (
    <View
      style={{
        flex: 1,
        position: "absolute",
        overflow: "hidden",
        height: "100%",
        width: "100%",
        backgroundColor: "#fff"
      }}
    >
      <Text
        style={{
          textAlign: "center",
          fontSize: 20,
          color: "#666",
          marginTop: 20
        }}
      >
        Sign below
      </Text>

      <SignaturePadEl
        onError={error}
        onChange={change}
        style={{
          flex: 1,
          backgroundColor: "#fff"
        }}
      />

      <StyledButton onPress={toggleShow} style={{ borderRadius: 0 }}>
        <Text
          style={{
            color: "#fff"
          }}
        >
          Done
        </Text>
      </StyledButton>
    </View>
  ) : null;
}

export default SignaturePadNative;
