import React from "react";
import SignatureCanvas from "react-signature-canvas";

import DialogTitle from "@material-ui/core/DialogTitle/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import DialogActions from "@material-ui/core/DialogActions/index";
import Dialog from "@material-ui/core/Dialog/index";
import Button from "@material-ui/core/Button/index";

import "./signaturePad.scss";

function SignaturePad({ toggleShow, open, saveSignatureImg }) {
  const sigCanvas = React.createRef();
  const handleDone = () => {
    const imgString = sigCanvas.current.toDataURL();

    // console.log(imgString);
    saveSignatureImg(imgString);
    toggleShow();
  };

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="sm"
      fullWidth={true}
      aria-labelledby="confirmation-dialog-title"
      open={open}
    >
      <DialogTitle id="confirmation-dialog-title">Sign below</DialogTitle>
      <DialogContent
        dividers
        style={{
          display: "flex",
          justifyContent: "center",
          overflow: "hidden"
        }}
      >
        <SignatureCanvas
          ref={sigCanvas}
          penColor="black"
          canvasProps={{ width: 500, height: 200, className: "sigCanvas" }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={toggleShow} color="primary">
          Cancel
        </Button>
        <Button onClick={handleDone} color="primary">
          Done
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default SignaturePad;
