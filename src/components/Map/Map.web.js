import React, { useState, useEffect } from "react";
import {
  Map as LeafletMap,
  TileLayer,
  Polyline,
  Marker,
  Popup
} from "react-leaflet";
import { Box, Typography } from "@material-ui/core/index";

import { createStartMarker, createStopMarker } from "../../helpers";
import "../map.scss";

function Map({ route, polyline, current }) {
  const [bounds, setBounds] = useState(null);

  const setMapBounds = () => {
    let latLngs = route.activities.map(el =>
      window.L.latLng(el.address.lat, el.address.lng)
    );

    let bounds = window.L.latLngBounds(latLngs);

    setBounds(bounds);

    setTimeout(() => setBounds(null), 2000);
  };

  // Prevent map from refitting bounds
  useEffect(() => setBounds(null), []);

  useEffect(() => {
    route.activities && setMapBounds();
    console.log("Polyline changed");
  }, [polyline]);

  return (
    <LeafletMap
      center={[52.20221513, 5.61392784]}
      zoom={7}
      bounds={bounds}
      boundsOptions={{ padding: [10, 10] }}
      maxZoom={20}
      attributionControl={true}
      zoomControl={true}
      doubleClickZoom={true}
      scrollWheelZoom={true}
      dragging={true}
      animate={true}
      easeLinearity={0.35}
    >
      {/* Polyline */}
      {polyline && (
        <Polyline
          key={Math.random()}
          positions={polyline.map(el => [el.lat, el.lng])}
          weight={3}
          opacity={0.8}
        />
      )}

      {/* Markers */}
      {route &&
        route.activities &&
        route.activities.map((address, i) => (
          <Marker
            key={i}
            position={[address.address.lat, address.address.lng]}
            icon={window.L.divIcon(
              address.type === "start"
                ? {
                    iconSize: [44, 54],
                    iconAnchor: [20, 57],
                    html: `
                            <div>
                              <img src="data:image/svg+xml;base64,${btoa(
                                createStartMarker("#5CA0FF")
                              )}">
                            </div>`
                  }
                : {
                    iconSize: [38, 45],
                    iconAnchor: [20, 35],
                    html: `
                            <div>
                              <img src="data:image/svg+xml;base64,${btoa(
                                createStopMarker("#5CA0FF")
                              )}">
                              <span>${
                                i !== 0 &&
                                i !== route.activities.length &&
                                !(
                                  address.address.lat ===
                                    route.activities[i - 1].address.lat &&
                                  address.address.lng ===
                                    route.activities[i - 1].address.lng
                                )
                                  ? String(i)
                                  : " "
                              }</span>
                            </div>`
                  }
            )}
          >
            <Popup>
              <Box>
                {address.information && address.information.name && (
                  <Typography variant="body2">
                    Name: {address.information.name}
                  </Typography>
                )}
                {address.information && address.information.zip && (
                  <Typography variant="body2">
                    Zip: {address.information.zip}
                  </Typography>
                )}
                {address.information &&
                  (address.information.city || address.information.street) && (
                    <Typography variant="body2">
                      Address:{" "}
                      {address.information.street &&
                        address.information.street + " "}{" "}
                      {address.information.city}
                    </Typography>
                  )}

                {!address.information && (
                  <Typography variant="body2">No info</Typography>
                )}
              </Box>
            </Popup>
          </Marker>
        ))}

      {/* Current Position Marker */}
      {current && (
        <Marker
          position={[current.lat, current.lng]}
          icon={window.L.divIcon({
            iconSize: [38, 45],
            iconAnchor: [20, 35],
            html: `
                      <div>
                        <img src="data:image/svg+xml;base64,${btoa(
                          createStopMarker("#5CA0FF")
                        )}">
                        <span>Me</span>
                      </div>`
          })}
          style={{
            width: 90,
            height: 90
          }}
          anchor={{
            x: 0.1,
            y: 0.2
          }}
        />
      )}

      <TileLayer url="https://api.mapbox.com/styles/v1/torosyaneric/cjwlu6x902p9y1cod4zsoetrl/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9yb3N5YW5lcmljIiwiYSI6ImNqd2tlNzYwNjBqcnk0NHA2azRvd2Fzc2cifQ.BvrMZuPXQBsfZHKXS9Ssug" />
    </LeafletMap>
  );
}

export default React.memo(Map);
