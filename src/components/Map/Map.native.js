import React, { useEffect } from "react";
import { Platform, View, Text, Dimensions } from "react-native";
import MapView, { Marker, Polyline } from "react-native-maps";

import stopIcon from "../../static/img/icons/marker-blue.png";
import startIcon from "../../static/img/icons/marker-blue-driver.png";

const { height, width } = Dimensions.get("window");
const LATITUDE_DELTA = 3;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);

function Map({ route, polyline, current }) {
  const map = React.createRef();

  const fitBounds = () => {
    const markers = route.activities.map(address => ({
      latitude: address.address.lat,
      longitude: address.address.lng
    }));

    map.current.fitToCoordinates(markers, {
      animated: true,
      edgePadding: { top: 25, right: 25, bottom: 25, left: 25 }
    });

    console.log(map.current);
  };

  useEffect(() => {
    route.activities && fitBounds();
  }, [route]);

  return (
    <MapView
      ref={map}
      style={{ height: "40%" }}
      initialRegion={{
        latitude: 52.20221513,
        longitude: 5.61392784,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }}
    >
      {/* Polyline */}
      {polyline && (
        <Polyline
          coordinates={polyline.map(point => ({
            latitude: point.lat,
            longitude: point.lng
          }))}
          strokeColor="#0082C8" // fallback for when `strokeColors` is not supported by the map-provider
          strokeWidth={3}
        />
      )}

      {/* Markers */}
      {route &&
        route.activities &&
        route.activities.map(
          (address, i, arr) =>
            !(
              i === arr.length - 1 &&
              (address.address.lat === arr[0].address.lat &&
                address.address.lon === arr[0].address.lon)
            ) && (
              <Marker
                key={i}
                coordinate={{
                  latitude: address.address.lat,
                  longitude: address.address.lon
                }}
                image={address.type === "start" ? startIcon : stopIcon}
                style={{
                  width: 90,
                  height: 90
                }}
                anchor={{
                  x: address.type === "start" ? 0.2 : 0.1,
                  y: address.type === "start" ? 0.8 : 0.2
                }}
              >
                {i !== 0 && (
                  <View
                    style={{
                      width: Platform.OS === "android" ? 22 : 31,
                      height: Platform.OS === "android" ? 25 : 33,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ fontSize: 10 }}>{i}</Text>
                  </View>
                )}
              </Marker>
            )
        )}

      {/* Current Position Marker */}
      {current && (
        <Marker
          coordinate={{
            latitude: current.lat,
            longitude: current.lng
          }}
          image={stopIcon}
          style={{
            width: 90,
            height: 90
          }}
          anchor={{
            x: 0.1,
            y: 0.2
          }}
        >
          <View
            style={{
              width: Platform.OS === "android" ? 22 : 31,
              height: Platform.OS === "android" ? 25 : 33,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ fontSize: 10 }}>Me</Text>
          </View>
        </Marker>
      )}
    </MapView>
  );
}

export default React.memo(Map);
