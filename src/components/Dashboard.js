import React, { Component } from "react";
import {
  Platform,
  View,
  FlatList,
  ScrollView,
  Text,
  Image,
  StyleSheet
} from "react-native";
import * as firebase from "firebase";
import getDirections from "react-native-google-maps-directions";
import Box from "@material-ui/core/Box";

import { firebaseApp } from "../firebase";
import Map from "./Map/Map";
import SignaturePad from "./SignaturePad/SignaturePad";
import BarcodeCam from "./BarcodeCam/BarcodeCam";
import Camera from "./Camera/Camera";
import StyledButton from "./StyledButton";

const decodePolyline = require("@mapbox/polyline");

const btnShadow = StyleSheet.create({
  btn: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.12,
    shadowRadius: 3,
    elevation: 5
  }
});

const dataURLtoFile = (dataurl, filename) => {
  let arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, { type: mime });
};

class Dashboard extends Component {
  state = {
    route: {},
    activeStop: -1,
    currentPosition: null,
    polyline: null,
    signaturePad: false,
    showBarcodeCam: false,
    showCam: false,
    barcode: null
  };

  decodeMap = () => {
    // Fetching OSRM routes
    const baseUrl = "http://209.250.232.107:5000/route/v1/driving/";

    let queryStr = this.state.route.activities
      .reduce((prev, el) => prev + `;${el.address.lng},${el.address.lat}`, "")
      .slice(1);

    return fetch(baseUrl + queryStr + "?overview=full")
      .then(res => res.json())
      .then(json => {
        // -- Decode with polyline lib --
        const decodedPath = decodePolyline
          .decode(json.routes[0].geometry)
          .map(el => ({ lat: el[0], lng: el[1] }));

        console.log(decodedPath);
        this.setState({ polyline: decodedPath });
      });
  };

  capitalize = txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();

  truncate = (str, limit, after) => {
    let content = str.trim();

    content =
      content.slice(0, limit) +
      (str.length > limit ? (after ? after : "") : "");
    return content;
  };

  goToMap = address => {
    navigator.geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        const lng = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const lat = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json

        this.setState({ currentPosition: { lat, lng } }, () => {
          const data = {
            source: {
              latitude: lat,
              longitude: lng
            },
            destination: {
              latitude: address.lat,
              longitude: address.lng
            },
            params: [
              {
                key: "travelmode",
                value: "driving"
              }
            ]
          };

          console.log(data.source);

          getDirections(data);
        });
      },
      error => alert(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
      }
    );
  };

  _signaturePadError = error => {
    console.error(error);
  };

  _signaturePadChange = ({ base64DataUrl }) => {
    console.log("Got new signature: " + base64DataUrl);
  };

  // Save signature image to Firebase
  saveSignatureImg = (str, activeStop) => {
    const storageRef = firebaseApp.storage().ref();
    const routeId = window.location.pathname.slice(1);

    const ref = storageRef.child(
      `signatureImages/sign-${routeId}-${activeStop}.png`
    );

    const file = dataURLtoFile(str, "image.png");

    ref.put(file).then(snapshot => {
      console.log("Uploaded a blob or file!");

      ref.getDownloadURL().then(url => {
        console.log(url);
        const { route } = this.state;
        route.activities[activeStop].signatureImg = url;

        this.setState({ route });

        this.postRoute(routeId, route);
      });
    });

    console.log(activeStop);
  };

  // Save camera image to Firebase
  saveCamImg = (str, activeStop) => {
    const storageRef = firebaseApp.storage().ref();
    const routeId = window.location.pathname.slice(1);

    const ref = storageRef.child(
      `signatureImages/img-${routeId}-${activeStop}.png`
    );

    const file = dataURLtoFile(str, "image.png");

    ref.put(file).then(snapshot => {
      console.log("Uploaded a blob or file!");

      ref.getDownloadURL().then(url => {
        console.log(url);
        const { route } = this.state;
        route.activities[activeStop].img = url;

        this.setState({ route });

        this.postRoute(routeId, route);
      });
    });

    console.log(activeStop);
  };

  postRoute = (doc, route) => {
    const db = firebaseApp.firestore();

    db.collection("drivers")
      .doc(doc)
      .update(route);
  };

  handleDone = index => {
    const { route } = this.state;

    if (this.state.activeStop >= index) {
      delete route.activities[index].img;
      delete route.activities[index].signatureImg;
    }

    if (index === 0) {
      route.startTime = firebase.firestore.Timestamp.fromDate(new Date());
    } else if (index === route.activities.length - 1) {
      route.finishTime = firebase.firestore.Timestamp.fromDate(new Date());
      route.finishLocation = this.state.currentPosition;
    }

    this.setState(
      prevState =>
        prevState.activeStop < index
          ? {
              activeStop: index
            }
          : {
              activeStop: index - 1
            },
      () => {
        const routeId = window.location.pathname.slice(1);
        route.activeStop = this.state.activeStop;

        this.postRoute(routeId, route);
      }
    );
  };

  componentDidMount() {
    const db = firebaseApp.firestore();
    const id = window.location.pathname.slice(1);

    const fetchData = async () => {
      let doc = await db
        .collection("drivers")
        .doc(id)
        .get();

      this.setState(
        {
          route: doc.data(),
          activeStop:
            doc.data().activeStop !== undefined ? doc.data().activeStop : -1
        },
        () => this.decodeMap()
      );
    };

    id && fetchData();

    // Realtime updates causes map to rerender and refit bounds - polyline value updates
    // db.collection("drivers")
    //   .doc(id)
    //   .onSnapshot(doc =>
    //     this.setState(
    //       {
    //         route: {
    //           activities: doc.data().activities,
    //           distance: doc.data().distance,
    //           duration: doc.data().duration
    //         }
    //       },
    //       () => this.decodeMap()
    //     )
    //   );

    //  Get and save current position every 5 seconds
    const setCurrLocation = () =>
      navigator.geolocation.getCurrentPosition(
        //Will give you the current location
        position => {
          const lng = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const lat = JSON.stringify(position.coords.latitude);
          //getting the Latitude from the location json

          this.setState({ currentPosition: { lat, lng } }, () => {
            console.log(this.state.currentPosition);

            db.collection("drivers")
              .doc(id)
              .update({
                currentLocation: this.state.currentPosition,
                lastUpdated: firebase.firestore.Timestamp.fromDate(new Date())
              });
          });
        },
        error => console.log(error.message),
        {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 1000
        }
      );

    setCurrLocation();
    setInterval(setCurrLocation, 5000);
  }

  render() {
    return (
      <View>
        <Map
          route={this.state.route}
          polyline={this.state.polyline}
          current={this.state.currentPosition}
        />

        <ScrollView style={{ height: "60%" }}>
          {Object.keys(this.state.route).length
            ? [
                <Box
                  key={0}
                  display={"flex"}
                  alignItems="center"
                  flexWrap="wrap"
                  justifyContent="space-between"
                  px={2}
                >
                  <View style={{ paddingVertical: 10, marginRight: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: "700" }}>
                      Route
                    </Text>

                    <Text>
                      {this.state.route.activities.length} stops •{" "}
                      {Math.floor(this.state.route.distance / 1000)}Km •{" "}
                      {this.state.route.distance % 1000}m
                    </Text>
                  </View>

                  <Box my={1}>
                    <StyledButton
                      style={btnShadow.btn}
                      onPress={() => {
                        this.setState({ showCam: true });
                      }}
                    >
                      <Text style={{ color: "#fff" }}>Scan barcode</Text>
                    </StyledButton>

                    {this.state.barcode && (
                      <Text>Last scanned: {this.state.barcode}</Text>
                    )}
                  </Box>
                </Box>,

                <View
                  key={1}
                  style={{
                    borderBottomColor: "#eee",
                    borderBottomWidth: 1
                  }}
                />,

                <FlatList
                  key={2}
                  data={this.state.route.activities}
                  style={{
                    paddingVertical: 16,
                    maxHeight:
                      Platform.OS === "web" ? "calc(60vh - 66px)" : null
                  }}
                  keyExtractor={(item, index) => String(index)}
                  renderItem={({ item, index }) => (
                    <View style={{ marginBottom: 6 }}>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ width: 80 }}>
                          <Text
                            style={{
                              minWidth: 36,
                              textAlign: "center",
                              marginBottom: 6,
                              opacity:
                                index > this.state.activeStop + 1 ? 0.5 : 1,
                              color: "#35e",
                              fontWeight: "800"
                            }}
                          >
                            {index === 0 ||
                            index === this.state.route.activities.length - 1
                              ? "◦"
                              : index}
                          </Text>

                          <View
                            style={{
                              width: 2,
                              backgroundColor: "#35e",
                              opacity: index > this.state.activeStop ? 0.5 : 1,
                              flex: 1,
                              alignSelf: "center"
                            }}
                          />
                        </View>

                        <View
                          style={{
                            flex: 1,
                            opacity: index > this.state.activeStop + 1 ? 0.5 : 1
                          }}
                        >
                          <Text style={{ marginBottom: 20 }}>
                            {item.information
                              ? item.information.name
                                ? this.truncate(
                                    item.information.name,
                                    35,
                                    "..."
                                  )
                                : "－"
                              : "－"}
                          </Text>

                          <View style={{ flexDirection: "row" }}>
                            {/* Signature */}
                            <View style={{ marginRight: 8 }}>
                              {index === this.state.activeStop + 1 &&
                                !item.signatureImg && (
                                  <View>
                                    <StyledButton
                                      onPress={() =>
                                        this.setState({ signaturePad: true })
                                      }
                                      style={{ width: 60, borderColor: "#ccc" }}
                                      outline
                                    >
                                      <Text>Sign</Text>
                                    </StyledButton>
                                  </View>
                                )}

                              {item.signatureImg ? (
                                <View
                                  style={{
                                    width: 80,
                                    height: 37,
                                    borderWidth: 1,
                                    borderRadius: 4,
                                    borderStyle: "dashed"
                                  }}
                                  accessibilityRole="link"
                                  href={item.signatureImg}
                                  target="_blank"
                                >
                                  <Image
                                    style={{
                                      width: "100%",
                                      height: "100%",
                                      resizeMode: "contain"
                                    }}
                                    source={{ uri: item.signatureImg }}
                                  />
                                </View>
                              ) : null}
                            </View>

                            {/* Photo */}
                            <View>
                              {index === this.state.activeStop + 1 &&
                                !item.img && (
                                  <View>
                                    <StyledButton
                                      onPress={() =>
                                        this.setState({ showCam: true })
                                      }
                                      style={{ width: 60, borderColor: "#ccc" }}
                                      outline
                                    >
                                      <Text>+Img</Text>
                                    </StyledButton>
                                  </View>
                                )}

                              {item.img ? (
                                <View
                                  style={{
                                    width: 80,
                                    height: 37,
                                    borderWidth: 1,
                                    borderRadius: 4
                                  }}
                                  accessibilityRole="link"
                                  href={item.img}
                                  target="_blank"
                                >
                                  <Image
                                    style={{
                                      width: "100%",
                                      height: "100%",
                                      resizeMode: "contain"
                                    }}
                                    source={{ uri: item.img }}
                                  />
                                </View>
                              ) : null}
                            </View>
                          </View>

                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "center",
                              marginVertical: 8
                            }}
                          >
                            <StyledButton
                              disabled={index > this.state.activeStop + 1}
                              style={
                                index <= this.state.activeStop + 1
                                  ? { ...btnShadow.btn }
                                  : null
                              }
                              onPress={() => this.handleDone(index)}
                              outline={index >= this.state.activeStop + 1}
                            >
                              <Text
                                style={{
                                  color:
                                    index > this.state.activeStop
                                      ? "#35e"
                                      : "#fff"
                                }}
                              >
                                {index === 0
                                  ? "Start"
                                  : index ===
                                    this.state.route.activities.length - 1
                                  ? "Finish"
                                  : "Done"}
                              </Text>
                            </StyledButton>

                            <StyledButton
                              color={"#ec3"}
                              style={
                                index <= this.state.activeStop + 1
                                  ? { ...btnShadow.btn, marginLeft: 8 }
                                  : { marginLeft: 8 }
                              }
                              disabled={index > this.state.activeStop + 1}
                              onPress={() => {
                                this.goToMap(item.address);
                              }}
                              outline={index >= this.state.activeStop + 1}
                            >
                              <Text
                                style={{
                                  color:
                                    index > this.state.activeStop
                                      ? "#ec3"
                                      : "#fff"
                                }}
                              >
                                Navigate
                              </Text>
                            </StyledButton>
                          </View>
                        </View>
                      </View>
                    </View>
                  )}
                />
              ]
            : null}
        </ScrollView>

        <SignaturePad
          error={this._signaturePadError}
          change={this._signaturePadChange}
          toggleShow={() => this.setState({ signaturePad: false })}
          open={this.state.signaturePad}
          saveSignatureImg={str =>
            this.saveSignatureImg(str, this.state.activeStop + 1)
          }
        />

        <BarcodeCam
          toggleShow={() => this.setState({ showBarcodeCam: false })}
          open={this.state.showBarcodeCam}
          setBarcode={val => this.setState({ barcode: val })}
        />

        <Camera
          toggleShow={() => this.setState({ showCam: false })}
          open={this.state.showCam}
          saveCamImg={str => this.saveCamImg(str, this.state.activeStop + 1)}
        />
      </View>
    );
  }
}

export default Dashboard;
