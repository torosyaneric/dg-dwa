import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog/index";
import DialogTitle from "@material-ui/core/DialogTitle/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import DialogActions from "@material-ui/core/DialogActions/index";
import Button from "@material-ui/core/Button/index";
import Box from "@material-ui/core/Box";
import Webcam from "react-webcam";

function BarcodeCam({ toggleShow, open, saveCamImg }) {
  const handleCapture = () => {
    const imageSrc = webcam.current.getScreenshot();

    saveCamImg(imageSrc);
    toggleShow();
  };

  const videoConstraints = {
    width: 1280,
    height: 720,
    facingMode: 'environment'
  };

  const webcam = React.useRef(null);

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="sm"
      fullWidth={true}
      aria-labelledby="confirmation-dialog-title"
      open={open}
    >
      <DialogTitle id="confirmation-dialog-title">Take a photo</DialogTitle>
      <DialogContent
        dividers
        style={{
          display: "flex",
          justifyContent: "center",
          overflow: "hidden"
        }}
      >
        <Box px={3} display="flex" alignItems="center" flexDirection="column">
          <Webcam
            audio={false}
            height={350}
            ref={webcam}
            screenshotFormat="image/jpeg"
            videoConstraints={videoConstraints}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          color="primary"
          onClick={handleCapture}
          style={{ marginRight: 8 }}
        >
          Capture photo
        </Button>
        <Button onClick={toggleShow} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default BarcodeCam;
