import React from "react";
import styled from "styled-components";
import { TouchableOpacity } from "react-native";

const StyledButton = styled(TouchableOpacity)`
  min-width: 120px;
  background-color: ${props =>
    props.outline ? "#fff" : props.color || "#35e"};
  border-width: 2px;
  border-color: ${props => props.color || "#35e"};
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  display: flex;
  text-transform: uppercase;
  font-weight: 600;
  padding: 8px;
`;

export default StyledButton;
