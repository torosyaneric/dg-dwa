import React, { Component } from "react";
import {
  Barcode,
  BarcodePicker,
  CameraAccess,
  CameraSettings,
  ScanSettings
} from "scandit-sdk/build/main/index";
import ScanditBarcodeScanner from "scandit-sdk-react";
import Dialog from "@material-ui/core/Dialog/index";
import DialogTitle from "@material-ui/core/DialogTitle/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import DialogActions from "@material-ui/core/DialogActions/index";
import Button from "@material-ui/core/Button/index";

function BarcodeCam({ toggleShow, open, setBarcode }) {
  const getScanSettings = () => {
    return new ScanSettings({
      enabledSymbologies: [
        Barcode.Symbology.EAN8,
        Barcode.Symbology.EAN13,
        Barcode.Symbology.UPCA,
        Barcode.Symbology.UPCE,
        Barcode.Symbology.CODE128,
        Barcode.Symbology.CODE39,
        Barcode.Symbology.CODE93,
        Barcode.Symbology.INTERLEAVED_2_OF_5
      ]
    });
  };

  const handleScan = data => {
    console.log(data.barcodes[0]);
    toggleShow();
    setBarcode(data.barcodes[0].data);
  };

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="sm"
      fullWidth={true}
      aria-labelledby="confirmation-dialog-title"
      open={open}
    >
      <DialogTitle id="confirmation-dialog-title">Scan a barcode</DialogTitle>
      <DialogContent
        dividers
        style={{
          display: "flex",
          justifyContent: "center",
          overflow: "hidden"
        }}
      >
        <ScanditBarcodeScanner
          // Library licensing & configuration options (see https://docs.scandit.com/stable/web/globals.html#configure)
          // AZ89hDOzLjqmMAbzMAY90r9Cv7cjK9PQPkE3Fh10/SfmfcQfMEtV1INBZsSRSEfGMApUyEJQlzgYW4p0YVsWaVk6N0cCfrUD5X/MnOpu8BXrQrtsAAZwIt2KvlUUUy0UnICoiOp1+XTCX0rargFEwcsS9el81Cfj0BTWUSo0vw0xHqeTprpWmjIKPbkKqVyhsxCLYBbgelOU1hE6BabblzXRudxzb331X55ZAU58fDbgU8uIOks003J2qZ2DzLnx/YODG/vElmCp3ZF1K8zTaGkKKhPoMcbZzun8wqSS8egYR6Nbtr+MPDIzlPZba5GCGRs0Fnefs3V5SziW4jGEKbB7ltLGsB+69bdWBsye0qMfsSakXyiS+1pyQDklMZk13Zr/tpwAD5uy0O5yKimLk6ut5AnV46L1y4Aqeg+ATd2vGgcsXfpQxZj6LPsez5KeU2DhmMMyITwKv8/HW2Ydu4Rl7ruXCuKh67dibCHQfV43IMZEXj0V6rqOv+Tw1EgIa6ej2VEIzxEHJ4povRs2yq1VQ8GqXFfdkROGUR7CPYmkwmnwFsZyaAVNWzfio4wuaxSM+Ux05XGSFBP4yo7SYR9rVMgO+b+MD87gow8baxG2tSKt9Vk3wlU/gJRpAXFNFCw1gJjId93wD+8+q4Swux8mWkACEzbQVtbdmldF8M3jPwraGjo9xypzwRebPhczgXqYc5P28X32xCMEO9xT6RAQOQU2uMHcfP4xPBohVejgo8cOjLRUafhpiwCGZ8enzk9NOCNOe5st3QSMh4ZJS616ibzNnl1aEA==
          licenseKey={
            "ARe9YxSzCfLKF5owsDj9ylkNWb7AJJD2DkcXkf4OnlcmW/TsEFV1Gwpx710eZK9wi0gp6rxhtizMZ+o8sG4+E05PYw6aP7gayWDrkhR390XNK4YUbWGGcnV3lSORZdJXS3sQSXVk/mXJBVDo1QTVfc4/XpW0gSdt5syjGRsTB5n/Vp4PM+zSJL/6LyRQjNqD+luKa31G9gMgv0HwKKGxZlCQOIjxcyvqBHuu1e/qotSsC6S0FOZwO/x0fimBeGaWrcNldKiLV8PsM43ih8hB0FWTIMbuaTASLCactejDcYY4pLfYWS0Ma8KBwmUziezAxV9W2nVTltjtwduOBZa6OqIulVJcI6g56lp5c+B0EzRAxKOwuO1MagtpXVvhRKlXP5iAydHQ8tR3cgP/PI+wXRwpGdlXX/umwakkpxzX0f8vjOtPodhekPoQSQEPTmbLUBxEWrc0NJEFcIk36SXaUNHVUM1sHKREAE7YIRBgXfkiKeN1+QtUeSaFWlgaIhdq8+tyvoEbc9k9AaM5n9vA9g5DBbN6zhwKJ71XEDHJFH3r+aKf2C9qXzpqBSS/5yY1QJkkYqlHBvNsjWJc1Z38dmhuOtgaoWP9eWXWjwL6vLVAI4NA4/0N/C+ucdvdhN3iWI5XgPSnuZwRZD48c7bPUIRdia4+/JZBYxgp/7y3Eitz5lfKTSESM6+9KUGHrqH6zhuQby5l+t+TXoqZQ6hGXuKDpbpF2wYu6ZgiBHEc865cgpD1uaGrSvjNA47L92XY6oX9/sDb40l1l1pXvsjTBnGgvqY62IC5qV/wXePSl1c2I+jCwa9SAIErCx0F1c0fU6g/"
          }
          engineLocation="https://unpkg.com/scandit-sdk@^3.1.0/build" // could also be a local folder, e.g. "build"
          // Picker events
          onScan={handleScan}
          onScanError={console.log}
          // Picker options
          scanSettings={getScanSettings()}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={toggleShow} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default BarcodeCam;
