import React, { Component } from "react";
import { Platform, Button, Image, StyleSheet, Text, View } from "react-native";
import Dashboard from "./src/components/Dashboard"
import {appStyles as styles} from "./src/App"
import Camera from "./src/components/Camera"

class App extends Component {
  render() {
    return (
      <View style={styles.app}>
        <Dashboard />
      </View>
    );
  }
}

export default App;
